var button = document.getElementById('form-click');
var form = document.getElementById('form');

var mapLink = document.querySelector(".button-map");
var mapPopup = document.querySelector(".modal-map");
var mapClose = document.querySelector(".modal-close");

button.onclick = function (e) {
	e.preventDefault();

	form.classList.contains('none') ? form.classList.remove('none') : form.classList.add('none')
};


mapLink.addEventListener("click", function (evt) {
	evt.preventDefault();
	mapPopup.classList.add("modal-show");
});

mapClose.addEventListener("click", function (evt) {
	evt.preventDefault();
	mapPopup.classList.remove("modal-show");
}),

window.addEventListener("keydown", function (evt) {
	evt.preventDefault();
	if (evt.keyCode == 27) {
		if (mapPopup.classList.contains("modal-show")) {
			mapPopup.classList.remove("modal-show");
		}
	}
});
